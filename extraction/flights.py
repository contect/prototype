import datetime
import os

import numpy as np
import pandas as pd

from common.constants import Column, Key
from common.fileio import read_csv_pandas, write_csv_pandas, read_csv_dask
from common.path import create_path

DTYPES = {
    'FLIGHT_ID': np.int,
    'SEGMENT_NO': np.int,
    'LATITUDE': np.double,
    'LONGITUDE': np.double,
    'ALTITUDE': np.double,
    'SEGMENT_MONTH': np.int,
    'SEGMENT_DAY': np.int,
    'SEGMENT_YEAR': np.int,
    'SEGMENT_HOUR': np.int,
    'SEGMENT_MIN': np.int,
    'SEGMENT_SEC': np.double,
    'EMISSIONS_MODE': np.double,
    'TEMPERATURE': np.double,
    'PRESSURE': np.double,
    'HUMIDITY': np.double,
    'SPEED': np.double,
    'SEGMENT_TIME': np.object,
    'TRACK_DISTANCE': np.double,
    'THRUST': np.double,
    'WEIGHT': np.double,
    'FUELBURN': np.double,
    'CO': np.double,
    'HC': np.double,
    'NOX': np.double,
    'PMNV': np.double,
    'PMSO': np.double,
    'PMFO': np.double,
    'CO2': np.double,
    'H2O': np.double,
    'SOX': np.double
}


def _processed_filepath(output_dir: str, granule: {}) -> str:
    starttime = datetime.datetime.strftime(
        granule[Column.start], "%Y%m%d_%H%M")
    endtime = datetime.datetime.strftime(granule[Column.end], "%Y%m%d_%H%M")
    filename = '_'.join(
        [granule[Column.h], granule[Column.v], starttime, endtime]) + '.csv'
    return create_path([output_dir, filename])


def _raw_filepath(input_dir, timestamp):
    filename = '_'.join([str(timestamp.month), str(
        timestamp.day), str(timestamp.year), 'SEGMENT']) + '.csv'
    return create_path([input_dir, filename])


def _filter_by_time_and_position(day_segment_df: pd.DataFrame, south: float, north: float, east: float, west: float,
                                 startdatetime: datetime, enddatetime: datetime):
    lat_min = min(south, north)
    lat_max = max(south, north)
    lon_min = min(west, east)
    lon_max = max(west, east)

    filtered = day_segment_df.loc[(day_segment_df['LATITUDE'] >= lat_min)
                                  & (day_segment_df['LATITUDE'] <= lat_max)
                                  & (day_segment_df['LONGITUDE'] >= lon_min)
                                  & (day_segment_df['LONGITUDE'] <= lon_max)
                                  & (day_segment_df['SEGMENT_HOUR'] >= startdatetime.hour)
                                  & (day_segment_df['SEGMENT_HOUR'] <= enddatetime.hour)
                                  & (day_segment_df['SEGMENT_MIN'] >= startdatetime.minute)
                                  & (day_segment_df['SEGMENT_MIN'] <= enddatetime.minute)
                                  ]
    computed = filtered.compute(scheduler='threads', num_workers=2)
    return computed


def extract_granule_flights(granule: {}, params: {}):
    processed_filepath = _processed_filepath(
        params[Key.in_flight_proc], granule)

    if os.path.exists(processed_filepath):
        processed = read_csv_pandas(processed_filepath, DTYPES)

    else:
        raw_filepath = _raw_filepath(
            params[Key.in_flight_raw], granule[Column.start])
        raw = read_csv_dask(raw_filepath, DTYPES, 1)

        if raw is not None:
            processed = _filter_by_time_and_position(raw, granule[Column.south], granule[Column.north],
                                                     granule[Column.east],
                                                     granule[Column.west],
                                                     granule[Column.start], granule[Column.end])
        else:
            processed = pd.DataFrame(DTYPES.keys())

        write_csv_pandas(processed, processed_filepath)

    granule[Column.flight_count] = len(
        processed['FLIGHT_ID'].unique().tolist())
    granule[Column.flight_file_processed] = processed_filepath

    del processed

    return granule
