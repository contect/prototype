"""
input: file list
output: metadata as df
"""

import datetime
import re

import pytz
from osgeo import gdal

from common.constants import Column


def get_geotransform(filepath: str):
    ds = get_hdf(filepath)
    return ds.GetGeoTransform()


def get_projection(filepath: str):
    ds = get_hdf(filepath)
    return ds.GetProjection()


def get_hdf(filepath: str):
    try:
        ds = gdal.Open(filepath)
        return ds
    except FileNotFoundError as e:
        print(e)


def get_hdf_metadata(filepath: str):
    ds = get_hdf(filepath)
    return ds.GetMetadata_Dict()


def get_sds_list(filepath: str):
    ds = get_hdf(filepath)
    return ds.GetSubDatasets()


def get_sds_path(filepath, band_number, band_keyword, band_suffix):
    path = None

    for sds_path, _ in get_sds_list(filepath):

        name = sds_path.split(':')[-1]
        sds_suffix = re.sub(
            '[^0-9]', '', name.replace(band_keyword, '').replace(band_suffix, ''))

        if name.find(band_keyword) != -1 and int(sds_suffix) == band_number:
            path = sds_path
            break

    return path


def get_sds_array(sds_path: str):
    try:
        sds = get_hdf(sds_path)
        return sds.ReadAsArray()
    except (FileNotFoundError, TypeError) as e:
        print(e)


def extract_metadata(filepath: str) -> {}:
    metadata = get_hdf_metadata(filepath)
    datetime_format = "%Y-%m-%dT%H:%M:%S.%fZ"

    granule = {Column.hdf: filepath,
               Column.name: filepath.split('/')[-1].replace('.hdf', ''),
               Column.start: datetime.datetime.strptime(metadata['GRANULEBEGINNINGDATETIME'],
                                                        datetime_format).astimezone(pytz.UTC),
               Column.end: datetime.datetime.strptime(metadata['GRANULEENDINGDATETIME'],
                                                      datetime_format).astimezone(pytz.UTC),
               Column.west: float(metadata['WESTBOUNDINGCOORDINATE']),
               Column.east: float(metadata['EASTBOUNDINGCOORDINATE']),
               Column.north: float(metadata['SOUTHBOUNDINGCOORDINATE']),
               Column.south: float(metadata['NORTHBOUNDINGCOORDINATE']),
               Column.shape: (int(metadata['DATACOLUMNS']), int(metadata['DATAROWS'])),
               Column.h: 'h' + metadata['HORIZONTALTILENUMBER'],
               Column.v: 'v' + metadata['VERTICALTILENUMBER']
               }
    del metadata

    return granule
