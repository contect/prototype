class Column:
    # product
    hdf = 'hdf'
    name = 'name'

    # metadata
    start = 'startdatetime'
    end = 'enddatetime'
    west = 'bound_west'
    east = 'bound_east'
    north = 'bound_north'
    south = 'bound_south'
    shape = 'dimension'
    bands = 'bands'
    h = 'horizontal_bounding'
    v = 'vertical_bounding'

    # flights
    flight_dir_raw = 'flight_raw'
    flight_dir_processed = 'flight_processed'
    flight_file_processed = 'flight_file_processed'
    flight_count = 'flight_count'

    # preprocessing
    btd = 'brightness_temperature'
    processed = 'processed_image'

    # detection
    mask = 'contrail_mask'
    intersects = 'intersections_per_block'
    line_types = 'line_type_intersections_per_block'
    cleaned = 'cleaned_image'

    # key values
    contrail_pixel = 'contrail_pixel'
    contrail_pixel_percent = 'contrail_pixel_percent'
    flight_pixel = 'flight_pixel'
    flight_pixel_percent = 'flight_pixel_percent'
    deviation = 'deviation'
    deviation_percent = 'deviation_percent'
    deviation_percent_abs = 'deviation_percent_abs'
    intersects_count = 'intersections_per_granule'
    line_types_count = 'line_types_per_granule'

    # classes
    category = 'category'
    action = 'recommended_action'

    # plots
    btd_path = 'btd_image'
    mask_path = 'mask_image'

    # runtime
    runtime = 'runtime_seconds'


file_columns = [Column.name,
                Column.hdf,
                Column.h,
                Column.v,
                Column.start,
                Column.end,
                Column.west,
                Column.east,
                Column.north,
                Column.south,
                Column.shape,
                Column.bands,
                Column.btd_path,
                Column.mask_path,
                Column.flight_file_processed,
                ]

result_columns = [Column.name,
                  Column.category,
                  Column.flight_count,
                  Column.flight_pixel,
                  Column.flight_pixel_percent,
                  Column.contrail_pixel,
                  Column.contrail_pixel_percent,
                  Column.deviation,
                  Column.deviation_percent,
                  Column.deviation_percent_abs,
                  Column.intersects_count,
                  Column.line_types_count,
                  Column.runtime
                  ]


class Category:
    NO_CONTRAIL = 0
    CONTRAIL_OK = 1
    TOO_MANY_CONTRAILS = 2
    TOO_FEW_CONTRAILS = 3


class Action:
    OK = 0
    HUMAN = 1


class Key:
    number_of_processes = 'process_number'

    # Path
    input_files = 'input_files'
    in_flight_raw = 'flight_raw_dir'
    in_flight_proc = 'flight_proc_dir'
    out_dir = 'output_dir'
    out_proc = 'processed_output'
    out_res = 'result_output'
    out_conf = 'config_output'

    # Bands
    bands = 'bands'
    band11 = '11um'
    band12 = '12um'
    band_key = 'band_key'
    band_suffix = 'band_suffix'

    # Preprocessing
    norm_interval = 'pp_norm_interval'
    kernel_size = 'pp_kernel_size'
    cda_k = 'pp_k'

    # Detection
    method = 'detection_method'
    min_px_size = 'detection_min_px_size'
    connectivity = 'connectivity'
    split_by = 'split_by'
    binary_threshold = 'binary_threshold'
    postprocessing = 'postprocessing'
    ratio_threshold = 'ratio_threshold'
    mask = 'mask'

    # Probabilistic
    line_length = 'probabilistic_line_length'
    line_gap = 'probabilistic_line_gap'
    ppht_threshold = 'probabilistic_threshold'

    # Standard
    sht_threshold = 'hough_threshold'

    # Classification
    cls_thresh_low = 'lower_classification_threshold'
    cls_thresh_high = 'upper_classification_threshold'
    flight_trail_width = 'flight_trail_width'
