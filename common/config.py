"""
config
"""

from configparser import ConfigParser
from datetime import date, datetime
from glob import glob
from os import cpu_count

from common.constants import Key
from common.path import check_path, create_path
from random import sample


def _today() -> str:
    return date.today().__str__()


def _now() -> str:
    return datetime.now().strftime("%Y%m%d-%H%M")


def _input_files(input_dir: str, limit: int) -> [str]:
    files = glob(input_dir + '/*.hdf')

    if limit > 0:
        files = sample(files, limit)

    return files


def _num_processes(use_mp: bool) -> int:
    if use_mp:
        num_processes = int(cpu_count() / 2)
    else:
        num_processes = 1
    return num_processes


def init_params(mask: str = None, method: str = None) -> {}:
    cp = ConfigParser()
    cp.read(check_path(['./config.ini'], create=False))

    if mask is None:
        mask = str(cp.get('detection', 'mask')).lower()

    if method is None:
        method = str(cp.get('detection', 'method'))

    limit = int(cp.get('general', 'limit'))
    run_dir = '_'.join([_now(), method, mask, str(limit)])
    out_root = str(cp.get('path', 'output_dir'))
    out_dir = check_path([out_root, run_dir], create=True)

    params = {
        # General
        Key.number_of_processes: _num_processes(bool(int(cp.get('general', 'multiprocessing')))),
        Key.input_files: _input_files(check_path([cp.get('path', 'hdf_dir'), cp.get(
            'product', 'name')], create=False), int(cp.get('general', 'limit'))),

        # Path
        Key.in_flight_raw: check_path([cp.get('path', 'flight_dir'), 'raw'], create=False),
        Key.in_flight_proc: check_path([cp.get('path', 'flight_dir'), 'processed'], create=True),
        Key.out_dir: out_dir,
        Key.out_proc: create_path([out_dir, 'processed_files.csv']),
        Key.out_res: create_path([out_dir, 'results.csv']),
        Key.out_conf: create_path([out_dir, 'config.json']),

        # Bands
        Key.bands: [int(band.strip()) for band in cp.get('product', 'bands').split(',')],
        Key.band11: int(cp.get('product', '11um')),
        Key.band12: int(cp.get('product', '12um')),
        Key.band_key: cp.get('product', 'band_keyword'),
        Key.band_suffix: cp.get('product', 'band_suffix'),

        # Preprocessing
        Key.norm_interval: int(cp.get('preprocessing', 'norm_interval')),
        Key.kernel_size: int(cp.get('preprocessing', 'gauss_kernel_size')),
        Key.cda_k: float(cp.get('preprocessing', 'k')),

        # Detection
        Key.method: method,
        Key.split_by: int(cp.get('detection', 'split_by')),
        Key.postprocessing: bool(int(cp.get('detection', 'postprocess'))),
        Key.mask: mask,
        Key.connectivity: int(cp.get('detection', 'connectivity')),

        # Classification
        Key.cls_thresh_low: float(cp.get('classification', 'low_threshold')),
        Key.cls_thresh_high: float(cp.get('classification', 'high_threshold')),
        Key.flight_trail_width: float(cp.get('classification', 'flight_trail_width')),
    }

    cp.read(check_path(['./mask_' + mask + '.ini'], False))
#    cp.read('./mask_' + mask + '.ini')
    mask_params = {
        Key.binary_threshold: int(cp.get('mask', 'binary_threshold')),
        Key.min_px_size: int(cp.get('mask', 'min_px_size')),
        Key.sht_threshold: float(cp.get('mask', 'sht_threshold')),
        Key.ppht_threshold: int(cp.get('mask', 'ppht_threshold')),
        Key.line_length: int(cp.get('mask', 'line_length')),
        Key.line_gap: int(cp.get('mask', 'line_gap')),
        Key.ratio_threshold: int(cp.get('mask', 'ratio_threshold'))
    }

    params.update(mask_params)

    return params
