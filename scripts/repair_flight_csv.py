import glob
import os
import re
import time
from collections import deque

import dask.dataframe as dd
import numpy as np
import pandas as pd

ROOT_DIR = 'data/flights/raw/*.csv'

DTYPES = {
    'FLIGHT_ID': int,
    'SEGMENT_NO': int,
    'LATITUDE': np.double,
    'LONGITUDE': np.double,
    'ALTITUDE': np.double,
    'SEGMENT_MONTH': int,
    'SEGMENT_DAY': int,
    'SEGMENT_YEAR': int,
    'SEGMENT_HOUR': int,
    'SEGMENT_MIN': int,
    'SEGMENT_SEC': np.double,
    'EMISSIONS_MODE': np.double,
    'TEMPERATURE': np.double,
    'PRESSURE': np.double,
    'HUMIDITY': np.double,
    'SPEED': np.double,
    'SEGMENT_TIME': object,
    'TRACK_DISTANCE': np.double,
    'THRUST': np.double,
    'WEIGHT': np.double,
    'FUELBURN': np.double,
    'CO': np.double,
    'HC': np.double,
    'NOX': np.double,
    'PMNV': np.double,
    'PMSO': np.double,
    'PMFO': np.double,
    'CO2': np.double,
    'H2O': np.double,
    'SOX': np.double
}


def _dd_read_csv(filename, data_types):
    return dd.read_csv(filename, skiprows=[1], dtype=data_types)


def _dd_write_csv(df, filename):
    dd.to_csv(df, filename)
    return os.path.exists(filename)


def _read_csv(filename, dtypes):
    return pd.read_csv(filename, dtype=dtypes)


def _write_csv(df: pd.DataFrame, filename: str):
    df.to_csv(filename, index=False)
    return os.path.exists(filename)


def _try_opening_csv(filepath_list: []):
    faulty_files = []

    for filepath in filepath_list:
        try:
            df = _dd_read_csv(filepath, DTYPES)
            df.tail(10)
        except:
            faulty_files.append(filepath)
            continue

    return faulty_files


def _check_last_line_in_csv(filepath_list: []):
    files_to_repair = []

    for filepath in filepath_list:
        with open(filepath, 'r') as f:
            if not re.match("^[0-9]+$", deque(f, 1)[0].split_blocks(',')[0]):
                files_to_repair.append(filepath)

    return files_to_repair


def _delete_last_line_in_csv(filepath_list: []):
    repaired_files = []

    for filepath in filepath_list:

        with open(filepath, "r+", encoding="utf-8") as file:

            # find end of file
            file.seek(0, os.SEEK_END)

            # skip the last character
            pos = file.tell() - 1

            # search for the newline character (i.e. the last row)
            while pos > 0 and file.read(1) != "\n":
                pos -= 1
                file.seek(pos, os.SEEK_SET)

            # delete the last line
            if pos > 0:
                file.seek(pos, os.SEEK_SET)
                file.truncate()

                repaired_files.append(filepath)

        # wait until operation is finished
        time.sleep(5)

    return repaired_files


def main():
    files_to_process = glob.glob(ROOT_DIR)
    faulty_files = _try_opening_csv(files_to_process)
    files_to_repair = _check_last_line_in_csv(faulty_files)
    repaired_files = _delete_last_line_in_csv(files_to_repair)
    print(repaired_files)


if __name__ == "__main__":
    main()
