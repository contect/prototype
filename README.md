# contect

Detecting contrails on MODIS satellite imagery with Hough transformation.

## Dependencies

- Python >= 3.8
- Conda

> Note: I recommend using conda over pip to avoid errors with gdal.

## Quick Start

1. Create conda environment and install requirements:

```shell
conda create --name contect-env python=3.8 --file requirements.txt
```

2. Activate environment:

```shell
conda activate contect-env
```

3. Change the parameters in `config.ini` to your needs.
4. Place HDF files according your settings in `config.ini`.
5. Place flight files according your settings in `config.ini`.
6. (optional) Check the flight files for errors:

```shell
python scripts/repair_flight_csv.py
```

7. Run:

```shell
python main.py
```

8. If you want to run all masks and all methods:
```shell
python run_all.py
```

## Folder structure

```
.
├── data/                 input and output data
...├── granules/
......├── MODTBGA/            
..........MODTBGA...hdf   satellite images as hdf
...├── flights/             
......├── processed/      filtered flights per granule as csv
......├── raw/            raw flights file as csv
├── analysis              analysis methods
├── common                config, logger, constants, ...
├── detection             hough, labelling, cleaning, block splitting
├── extraction            file extraction operations for hdf/csv
├── plot                  plotting
├── preprocessing         Contrail Detection Algorithm
├── scripts               useful scripts to run alone
├── config.ini            configuration
```

## Configuration

### General

```ini
[general]
multiprocessing = 1             # use multiprocessing
limit = 5                       # only process x random granules

[path]
hdf_dir = data/granules         # hdf root dir
flight_dir = data/flights       # flight csv root dir
output_dir = data/output        # output root dir

[product]
name = MODTBGA                  # name of the product
bands = 31, 32                  # bands to extract
11um = 31                       # band at 11um
12um = 32                       # band at 12um
band_keyword = BAND             # keyword to filter subdataset
band_suffix = _1                # suffix to remove

[preprocessing]
norm_interval = 100             # normalise images between 0 and ...
gauss_kernel_size = 5           # kernel size
k = 0.1                         # factor to add while calculating cda                  

[detection]
method = probabilistic          # method to use (probabilistic, standard)
mask = A                        # mask to use
split_by = 4                    # split image into x rows and x colums
postprocess = 0                 # do postprocessing with ppht
connectivity = 2                # connectivity in cleaning

[classification]
low_threshold = -0.5            # lower tolerance threshold
high_threshold = 0.5            # upper tolerance threshold
flight_trail_width = 10         # approx. width of flight trail in px
```

### Masks

Three masks are defined: A, B and C. A is the most sensitive, C the less.
You can add more masks by adding a `mask_x.ini`, where `x` is your mask number to use in the detection section in `config.ini`. The search is case-insensitive.

```ini
[mask]
binary_threshold = 50           # binary threshold in percent
min_px_size = 80                # size of min pixel size
sht_threshold = 0.3             # hough threshold for standard
ppht_threshold = 4              # hough threshold for probabilistic
line_length = 10                # line length for probabilistic
line_gap = 10                   # line gap for probabilistic
ratio_threshold = 3             # line filter threshold
```