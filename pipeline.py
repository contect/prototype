import time
from analysis import metrics
from common.constants import Column
from detection import hough
from extraction import flights, hdf
from plot import plot
from preprocessing import cda


def run(filepath: str, params: {}):

    start = time.time()

    """
    EXTRACTION
    """
    granule = hdf.extract_metadata(filepath)

    """
    FLIGHT FILTERING
    """
    granule = flights.extract_granule_flights(granule, params)

    """
    PREPROCESSING
    """
    granule = cda.preprocess_image(granule, params)

    """
    DETECTION
    """
    granule = hough.detect_lines_blockwise(granule, params)

    """
    ANALYSIS
    """
    granule = metrics.collect_metrics(granule, params)

    """
    PLOTTING
    """
    granule = plot.plot_contrail_mask_on_cleaned(granule, params)
    granule = plot.plot_contrail_mask(granule, params)

    end = time.time()
    granule[Column.runtime] = round(end - start, 2)
    print(granule[Column.name])

    return granule
