import numpy as np

"""
BLOCKS
"""


def create_block(img: np.array, row: int, column: int, size: int) -> np.array:
    base = np.arange(size)
    return img[base[:, np.newaxis] + row * size, base + column * size]


def split_blocks(img: np.array, split_by: int, block_size: int) -> [np.array]:
    blocks = []
    for row in range(split_by):
        for column in range(split_by):
            blocks.append(create_block(img, row, column, block_size))
    return blocks


def combine_blocks(blocks: [np.array], split_by: int) -> np.array:
    if split_by == 0:
        img = blocks[0]
    else:
        rows = [np.concatenate(blocks[col_num:col_num + split_by], axis=1) for col_num in
                range(0, len(blocks), split_by)]
        img = np.concatenate(rows, axis=0)
    return img
