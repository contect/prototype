import numpy as np
from skimage.transform import (hough_line, hough_line_peaks,
                               probabilistic_hough_line)

from common.constants import Column, Key
from detection.clean import (close_gaps_binary, filter_small_objects,
                             threshold_binary)
from detection.label import (filter_labels, get_line_label_intersections,
                             get_line_type_segments, label_segments,
                             line_segment_intersect_prob)
from detection.split import combine_blocks, split_blocks

"""
STRAIGHT LINE HOUGH TRANSFORMATION
"""


def standard(img: np.array, params: {}) -> np.array:
    tested_angles = np.linspace(-np.pi / 2, np.pi / 2, 360, endpoint=False)
    h, theta, d = hough_line(img, theta=tested_angles)
    line_peaks = hough_line_peaks(
        h, theta, d, threshold=params[Key.sht_threshold] * h.max())

    labeled = label_segments(img)
    intersect_labels = get_line_label_intersections(
        labeled, line_peaks, img.shape)
    line_type_labels = get_line_type_segments(
        labeled, intersect_labels, params[Key.ratio_threshold])
    mask = filter_labels(labeled, line_type_labels)

    mask = close_gaps_binary(mask)
    mask = filter_small_objects(mask, params[Key.min_px_size], True)

    return mask, len(intersect_labels), len(line_type_labels)


"""
PROBABILISTIC HOUGH TRANSFORMATION
"""


def probabilistic(img, params):
    lines = probabilistic_hough_line(
        img, threshold=params[Key.ppht_threshold], line_length=params[Key.line_length],
        line_gap=params[Key.line_gap])
    labeled = label_segments(img)
    intersect_labels = line_segment_intersect_prob(labeled, lines)
    line_type_labels = get_line_type_segments(
        labeled, intersect_labels, params[Key.ratio_threshold])
    mask = filter_labels(labeled, line_type_labels)

    return mask, len(intersect_labels), len(line_type_labels)


def detect_lines_blockwise(granule: {}, params: {}):
    if granule.get(Column.flight_count) <= 0:
        return granule

    img = granule.get(Column.processed)

    # Cleaning
    img = threshold_binary(img, params[Key.binary_threshold])
    img = filter_small_objects(img, params[Key.min_px_size])
    img = close_gaps_binary(img)

    split_by = 0

    # Split blocks
    if params[Key.split_by] >= 0:
        split_by = params[Key.split_by]
        block_size = granule.get(Column.shape)[0] // split_by
        blocks = split_blocks(img, split_by, block_size)
    else:
        blocks = [img]

    # Detection
    if params[Key.method] == 'probabilistic':
        masks, mask_intersection_count, mask_line_type_count = map(list, zip(*[probabilistic(block, params) for block in
                                                                               blocks]))
    else:
        masks, mask_intersection_count, mask_line_type_count = map(list, zip(*[standard(block, params) for block in
                                                                               blocks]))

    # Combine blocks
    mask = combine_blocks(masks, split_by)

    # Postprocess blocks
    if params[Key.postprocessing]:
        mask, mask_intersection_count, mask_line_type_count = probabilistic(
            mask, params)

    granule[Column.cleaned] = img
    granule[Column.mask] = mask
    granule[Column.intersects] = mask_intersection_count
    granule[Column.line_types] = mask_line_type_count

    del blocks
    del masks
    del mask

    return granule
