import pandas as pd

from common.constants import Action, Category, Column, Key


def _category(row: pd.Series, lower_limit: int, upper_limit: int):
    if row[Column.flight_count] == 0:
        return Category.NO_CONTRAIL
    if row[Column.deviation_percent] <= lower_limit:
        return Category.TOO_FEW_CONTRAILS
    if row[Column.deviation_percent] >= upper_limit:
        return Category.TOO_MANY_CONTRAILS
    if lower_limit < row[Column.deviation_percent] < upper_limit:
        return Category.CONTRAIL_OK


def _action(row: pd.Series):
    if row[Column.category] == Category.TOO_FEW_CONTRAILS or row[Column.category] == Category.TOO_MANY_CONTRAILS:
        return Action.HUMAN
    else:
        return Action.OK


def _get_first_rows_by_percentage(df: pd.DataFrame, row_num: int):
    return df.head(int(len(df) * (row_num / 100)))


def categorise(granules: [{}], params: {}) -> pd.DataFrame:
    df = pd.DataFrame(granules)
    lower_limit = params[Key.cls_thresh_low]
    upper_limit = params[Key.cls_thresh_high]
    df[Column.category] = df.apply(lambda row: _category(
        row, lower_limit, upper_limit), axis=1)
    df[Column.action] = df.apply(_action, axis=1)

    accuracy = len(df[df[Column.action] == Action.OK]) / len(df)

    print('ACCURACY: ' + str(accuracy))
    print('TOO MANY CONTRAILS: ' +
          str(len(df[df[Column.category] == Category.TOO_MANY_CONTRAILS])))
    print('TOO FEW CONTRAILS: ' +
          str(len(df[df[Column.category] == Category.TOO_FEW_CONTRAILS])))
    print('ACTION NEEDED: ' + str(len(df[df[Column.action] == Action.HUMAN])))

    df = df.fillna(0)
    return df
