import numpy as np
from shapely.geometry import LineString

from common.constants import Column, Key
from common.fileio import read_csv_pandas


def collect_metrics(granule: {}, params: {}):
    if granule[Column.flight_count] <= 0:
        return granule

    # contrails
    granule[Column.contrail_pixel] = granule[Column.mask].sum()
    granule[Column.contrail_pixel_percent] = (granule[Column.contrail_pixel] / (
        (granule[Column.shape][0] * granule[Column.shape][1]))) * 100

    # flights
    granule[Column.flight_pixel] = _flight_pixel(
        granule[Column.flight_file_processed], params[Key.flight_trail_width])
    granule[Column.flight_pixel_percent] = (granule[Column.flight_pixel] / (
        (granule[Column.shape][0] * granule[Column.shape][1]))) * 100

    # intersections
    granule[Column.intersects_count] = sum(granule[Column.intersects])
    granule[Column.line_types_count] = sum(granule[Column.line_types])

    # ratio between contrails and flights
    granule[Column.deviation] = granule[Column.contrail_pixel_percent] - \
        granule[Column.flight_pixel_percent]
    granule[Column.deviation_percent] = granule[Column.deviation] / \
        granule[Column.flight_pixel_percent]
    granule[Column.deviation_percent_abs] = np.abs(
        granule[Column.deviation_percent])

    return granule


def _flight_pixel(csv_path: str, dilation: float):
    flights = read_csv_pandas(csv_path)

    flight_area = 0

    for flight_id in flights['FLIGHT_ID'].unique():
        flight = flights[flights['FLIGHT_ID'] == flight_id]
        points = list(zip(flight['LATITUDE'], flight['LONGITUDE']))

        if len(points) > 2:
            line = LineString(points)
            dilated = line.buffer(dilation)

            flight_area += dilated.area

    return flight_area
