import matplotlib
import matplotlib.pyplot as plt

from common.constants import Column, Key
from common.path import create_path

matplotlib.use('Agg')

cm_gray = plt.cm.get_cmap("gray")
cm_bin = plt.cm.get_cmap("binary")
cm_contrail = plt.cm.get_cmap("Reds").copy()
cm_contrail.set_under('k', alpha=0)


def plot_contrail_mask_on_cleaned(granule, params):
    if granule[Column.flight_count] <= 0:
        return granule

    fig, ax = plt.subplots(figsize=(1, 1))
    ax.imshow(granule[Column.btd], cmap='gray')
    ax.imshow(granule[Column.mask],
              interpolation='nearest',
              cmap=cm_contrail, clim=[0.5, 1])
    ax.set_axis_off()

    filepath = create_path(
        [params[Key.out_dir], granule[Column.name] + '.png'])
    fig.savefig(filepath, bbox_inches='tight',
                pad_inches=0, dpi=granule[Column.shape][0])

    plt.close(fig)
    plt.clf()

    granule[Column.btd_path] = filepath

    return granule


def plot_contrail_mask(granule: {}, params: {}):
    if granule[Column.flight_count] <= 0:
        return granule

    fig, ax = plt.subplots()
    ax.imshow(granule[Column.mask], cmap='gray')
    ax.set_axis_off()

    filepath = create_path(
        [params[Key.out_dir], granule[Column.name] + '_mask.png'])
    fig.savefig(filepath, bbox_inches='tight',
                pad_inches=0, dpi=granule[Column.shape][0])

    plt.close(fig)
    plt.clf()

    granule[Column.mask_path] = filepath

    return granule
