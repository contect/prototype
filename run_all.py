"""
imports
"""
import time
from datetime import datetime

from main import main as run

def main():
    methods = ['standard', 'probabilistic']
    masks = ['a', 'b', 'c']
    start = time.time()

    for method in methods:
        for mask in masks:
            run(method, mask)

    end = time.time()
    run_time = round(end - start, 2)
    print("Overall time: " + str(run_time))

    filename = datetime.now().strftime("%Y%m%d%H%M") + '_time.txt'
    with open(filename, 'w') as f:
        f.write('starttime: ' + str(start) + 'sec \n' +
                'endtime: ' + str(end) + 'sec \n' +
                'runtime: ' + str(run_time)) + 'sec'

if __name__ == '__main__':
    main()
